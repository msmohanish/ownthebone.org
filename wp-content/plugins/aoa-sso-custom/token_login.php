<?php

//Execute this function if the cookie is set then logs the user based on the user ID.
add_action('init', 'token_login', 99);
function token_login() {
    
    //SF connection parameters
    $sys_username = get_field('username', 'option');
    $sys_password = get_field('password', 'option');
    
    try {
        
        $SF_User_Id = base64_decode($_GET['SF_User_Id']);
        $SF_Contact_Id = base64_decode($_GET['SF_UserContact_Id']);
        
        if($SF_User_Id != '' && $SF_Contact_Id != ''){
            
            require_once(get_stylesheet_directory() . '/sf_fonteva/soapclient/SforceEnterpriseClient.php' );
            
            $mySforceConnection = new SforceEnterpriseClient();
            $mySforceConnection->createConnection(site_url()."/sf_fonteva/soapclient/enterprise.wsdl.xml");
            $mySforceConnection->login($sys_username, $sys_password);
            
            $query = "SELECT username, firstname, lastname, email from User where Id = '" . $SF_User_Id . "'";
            $response = $mySforceConnection->query($query);
            
            if ($response->done == 1 && !empty($response->records)) { //response received.
                
                $email = (isset($response->records[0]->Email) ? $response->records[0]->Email : '');
                $username = (isset($response->records[0]->Username) ? $response->records[0]->Username : '');
                $firstname = (isset($response->records[0]->FirstName) ? $response->records[0]->FirstName : '');
                $lastname = (isset($response->records[0]->LastName) ? $response->records[0]->LastName : '');

                //get user by username
                $user = get_user_by('login', (string) $username);

                if (empty($user)) {
                    $user = get_user_by('email', $email); //get user by email
                }

                //user array
                $uData = [
                    'user_email' => strtolower($email),
                    'user_login' => strtolower($username),
                    'first_name' => trim($firstname),
                    'last_name' => trim($lastname),
                    'nickname' => trim($firstname) . " " . trim($lastname)
                ];

                //check if user is present in WP
                if (!is_object($user)) { //if not present then insert
                    $create = wp_insert_user($uData);
                    $user = new WP_User($create);
                } else {

                    $result = wp_update_user($uData + [
                        'ID' => $user->ID
                    ]);
                }
                
                //SF get login user contact details
                $query = "SELECT OrderApi__Contact__c, OrderAPi__Badge_Type__r.Name, OrderAPI__Is_Active__c from OrderApi__Badge__c where OrderApi__Contact__c = '" . $SF_Contact_Id . "'";
                $response_badge = $mySforceConnection->query($query);

                if(isset($response_badge) && !empty($response_badge)){

                    if($response_badge->done == 1){

                        $records = $response_badge->records;

                        $roles = [];
                        $badge_record = [];
                        foreach($records as $key => $value){

                            $roles[] = $value->OrderApi__Badge_Type__r->Name; //names

                            $badge_record['type'][] = $value->attributes->type;
                            $badge_record['is_active'][] = $value->OrderApi__Is_Active__c;
                            $badge_record['OrderApi__Contact__c'][] = $value->OrderApi__Contact__c;
                        }
                    }
                }

                //update user meta and add details
                $params = array(
                    'SF_User_Id' => $SF_User_Id,
                    'SF_UserContact_Id' => $SF_Contact_Id,
                    'Badge_Info' => $badge_record
                );

                // add to meta
                update_user_meta(
                    $user->ID, 'salesforce', $params
                );

                //update role
                //remove all previous roles of the user
                foreach ( $user->roles as $role ){
                    $user->remove_role($role);
                }

                if(!empty($roles)){
                    foreach($roles as $role){
                        $role_slug = sanitize_title($role);

                        add_role($role_slug, $role);
                        $user->add_role($role_slug);
                    }
                }
                
                //based on the above criterias make the user logs in to the system.
                if (!is_user_logged_in()) {
                    wp_set_current_user($user->ID, $user->user_login);
                    wp_set_auth_cookie($user->ID);
                    do_action('wp_login', $user->user_login);
                    wp_redirect(site_url());exit;
                }
            }
        }
        
    } catch (\Exception $ex) {

        error_log('Caught exception: ' . $ex->getMessage());
    }
}
?>
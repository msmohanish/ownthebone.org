<?php

global $wpdb;

$charset_collate = $wpdb->get_charset_collate();

$logout_table = $wpdb->prefix . 'custom_logout';

if ($wpdb->get_var("SHOW tables like '$logout_table'") != $logout_table) {
    
    $sql1 = "CREATE TABLE " . $logout_table . " (
                `id` int(4) NOT NULL AUTO_INCREMENT PRIMARY KEY,
                `ip_address` varchar(255) NOT NULL,
                `http_user_agent` varchar(255) NOT NULL,
                `http_forwarded_host` varchar(255) NOT NULL,
                `user_id` varchar(255) NOT NULL
             ) " . $charset_collate . ";";

    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
    dbDelta($sql1);
}
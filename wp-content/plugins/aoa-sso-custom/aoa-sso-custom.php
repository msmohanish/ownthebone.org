<?php
/*
  Plugin Name: AOA SSO Custom Setting
  Plugin URI: https://fusionspan.com
  Description: Plugin has all the related custom setting required for SSO and roles.
  Author: Fusionspan
  Version: 1.0
  Author URI: https://fusionspan.com
 */
?>
<?php

//create logout table
require_once('InstallUninstall.php');
include('token_login.php');

/**
 * for existing user
 */
add_action('wp_saml_auth_existing_user_authenticated',
    function( $existing_user,$attributes )
    {
        updateUserRoles($existing_user,$attributes);
        
}, 10, 2);

    
//if new user
add_action('wp_saml_auth_new_user_authenticated',
    function( $newuser,$attributes )
    {
        updateUserRoles($newuser,$attributes);
        
}, 20, 2);


//Update user roles based on user data
function updateUserRoles($userdata, $attributes){
    
    $UserContactId = $attributes['UserContactId'][0];
    $UserId = $attributes['userId'][0];
    $email = $attributes['email'][0];
    $username = $attributes['username'][0];
    
    //SF connection parameters
    $sys_username = get_field('username', 'option');
    $sys_password = get_field('password', 'option');

    try {
        
        require_once(get_stylesheet_directory() . '/sf_fonteva/soapclient/SforceEnterpriseClient.php' );
        
        $mySforceConnection = new SforceEnterpriseClient();
        
        $mySforceConnection->createConnection(site_url()."/sf_fonteva/soapclient/enterprise.wsdl.xml");
        
        $mySforceConnection->login($sys_username, $sys_password);
        
        //SF get login user contact details
        $query = "SELECT OrderApi__Contact__c, OrderAPi__Badge_Type__r.Name, OrderAPI__Is_Active__c from OrderApi__Badge__c where OrderApi__Contact__c = '" . $UserContactId . "'";
        $response_badge = $mySforceConnection->query($query);
        
        if(isset($response_badge) && !empty($response_badge)){
                
            if($response_badge->done == 1){

                $records = $response_badge->records;

                $roles = [];
                $badge_record = [];
                foreach($records as $key => $value){

                    $roles[] = $value->OrderApi__Badge_Type__r->Name; //names

                    $badge_record['type'][] = $value->attributes->type;
                    $badge_record['is_active'][] = $value->OrderApi__Is_Active__c;
                    $badge_record['OrderApi__Contact__c'][] = $value->OrderApi__Contact__c;
                }
            }
        }
        
        //get user by username
        $user = get_user_by('login', (string) $username);

        if (empty($user)) {
            $user = get_user_by('email', $email); //get user by email
        }
         
        //update user meta and add details
        $params = array(
            'SF_User_Id' => $UserId,
            'SF_UserContact_Id' => $UserContactId,
            'Badge_Info' => $badge_record
        );
        
        // add to meta
        update_user_meta(
            $user->ID, 'salesforce', $params
        );
        
        //update role
        //remove all previous roles of the user
        foreach ( $user->roles as $role ){
            $user->remove_role($role);
        }

        if(!empty($roles)){
            foreach($roles as $role){
                $role_slug = sanitize_title($role);

                add_role($role_slug, $role);
                $user->add_role($role_slug);
            }
        }
        
        //set SF variable in cookie
        $site_url = site_url();
        $site_url = preg_replace("(^https?://)", "", $site_url);
        @setcookie('SF_Login', 'YES', time() + 86400, '/', $site_url, false);
        
    } catch (\Exception $e) {
        echo $msg = $e->getMessage();
        exit;
    }
}

//It will be used to set custom setting for SF.
if( function_exists('acf_add_options_page') ) {
	
    acf_add_options_page(array(
        'page_title' 	=> 'SF Custom Setting',
        'menu_title'	=> 'SF Custom Setting',
        'menu_slug' 	=> 'sf-custom-settings',
        'capability'	=> 'edit_posts',
        'redirect'	=> false
    ));
}

//change the logout link to distinguish between normal WP user & SF User
//add_filter( 'logout_url', 'wpse_naea_logout_url' );
/*function wpse_naea_logout_url( $default ) 
{
    
    //get user roles
    $user_id = get_current_user_id();
    $user_meta = get_userdata($user_id);
    
    if(strpos($user_meta->data->user_login, '@') !== false){ //if email address
        
        $sf_logout_url = get_field('single_logout_service_url', 'option');
        
        return $sf_logout_url.'?RelayState='.$default;
    }
    
    return $default;
}*/

/*
 * Destroy SF cookie on logout
 */

function logOut() {
    
    if(isset($_COOKIE['SF_Login']) && $_COOKIE['SF_Login'] == 'YES'){
        
        $site_url = site_url();
        $site_url = preg_replace("(^https?://)", "", $site_url);
        setcookie('SF_Login', FALSE, -1, '/', $site_url);
        
        $sf_logout_url = get_field('single_logout_service_url', 'option');
        wp_redirect( $sf_logout_url ); exit;
    }
}
//add_action('wp_logout', 'logOut');
?>

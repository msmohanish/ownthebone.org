<?php
/**
 * The template for displaying any single page.
 *
 */
get_header(); // This fxn gets the header.php file and renders it 
?>
<div class="page-container">

    <div class="container"> 
        <div class="row">
            <div class="col-md-8 col-xs-12">
                <div class="content-wrapper">

                     

 
                    <?php while (have_posts()) : the_post() ?>

                        <h1 class="single-post-title"><?php the_title(); ?></h1>
                        <?php the_content(); ?>

                    <?php endwhile; ?>

                     
                </div>
            </div>
            
             <div class="col-md-4 col-xs-12" id="sidebar">
                 
               <div class="widget-area">
                    <h2>Sidebar </h2>
                </div><!-- widget-area -->

            </div>
        </div>
    </div>

</div><!-- .page-container -->

<?php
get_footer();

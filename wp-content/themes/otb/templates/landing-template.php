<?php
/**
 * The main template file
  Template Name: Landing Page Template
 *
 *
 */
get_header();

$page_id = get_the_ID();
?>

<div class="page-container landing-page-containe">
    <div class="container">
        <div class="main-content">  
            <?php
            if (have_posts()) {
                while (have_posts()) : the_post();
                    ?>            
                    <?php the_content(); ?>                
                <?php endwhile; ?>
            <?php } ?>


        </div>
    </div>
</div>
<?php
get_footer();


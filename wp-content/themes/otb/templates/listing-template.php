<?php
/**
 * The main template file
  Template Name: Listing Page Template
 *
 *
 */
get_header();
?>
<div class="banner-section test">
    <?php
    //Featured image
    if (has_post_thumbnail($page_id)) {
        the_post_thumbnail('full');
    }
    ?>
    
	<div class="container">
        <div class="banner-caption">
            <h1>List Page Header Text</h1>
        </div>
    </div>
</div>

<?php echo the_breadcrumb(); ?>

<div class="page-container landing-page-containe">
    <div class="container">
        <div class="main-content">  
           
		<h2 style="text-align:center; margin-bottom:30px;">List Items</h2>
        
        
<div class="listing-page">

                        
                        
<div class="listing-first-row row" style=" margin-left: 0;">

<div class="addbox col-md-3 col-xs-12 order-md-2">
	     <a href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/ad-for-listing-page.jpg" /></a>         
</div>


<div class="list-box col-md-9 order-md-1">
    <div class="listing-box-thumb col">
        <a href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/dummy-image.jpg" /></a>
    </div>
    <div class="listing-box-content">
        <h4><a href="#">List Item Name</a></h4>
        <h5>Attribute 1</h5>
        <h6>Attribute 2</h6>
        <p>Vestibulum eu erat vel quam malesuada tempor. Donec et luctus mauris. Cras ornare metus sit amet pellentesque tempus. Phasellus Donec et luctus mauris. Cras ornare...</p>
        <a class="link-with-right-arrow" href="#">Read More</a>
    </div>
    <div class="clear"></div>
</div><!-- list-box -->

</div><!--listing-first-row-->


	<div class="list-box">
    	<div class="listing-box-thumb col">
        	<a href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/dummy-image.jpg" /></a>
        </div>
        <div class="listing-box-content">
        	<h4><a href="#">List Item Name</a></h4>
            <h5>Attribute 1</h5>
            <h6>Attribute 2</h6>
            <p>Vestibulum eu erat vel quam malesuada tempor. Donec et luctus mauris. Cras ornare metus sit amet pellentesque tempus. Phasellus Donec et luctus mauris. Cras ornare...</p>
            <div class="moretext">
        	<p>Brisket ball tip cow sirloin. Chuck porchetta kielbasa pork chop doner sirloin, bacon beef brisket ball tip short ribs. </p>
            <p>Vestibulum eu erat vel quam malesuada tempor. Donec et luctus mauris. Cras ornare metus sit amet pellentesque tempus. Phasellus Donec et luctus mauris.</p>
        </div>
        <a class="link-with-right-arrow moreless-button" href="#readless">Read More</a> 
        </div>
        <div class="clear"></div>
    </div><!-- list-box -->
    
	<div class="list-box">
    	<div class="listing-box-thumb col">
        	<a href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/dummy-image.jpg" /></a>
        </div>
        <div class="listing-box-content">
        	<h4><a href="#">List Item Name</a></h4>
            <h5>Attribute 1</h5>
            <h6>Attribute 2</h6>
            <p>Vestibulum eu erat vel quam malesuada tempor. Donec et luctus mauris. Cras ornare metus sit amet pellentesque tempus. Phasellus Donec et luctus mauris. Cras ornare...</p>
            <a class="link-with-right-arrow" href="#">Read More</a>
        </div>
        <div class="clear"></div>
    </div><!-- list-box -->
    
	<div class="list-box">
    	<div class="listing-box-thumb col">
        	<a href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/dummy-image.jpg" /></a>
        </div>
        <div class="listing-box-content">
        	<h4><a href="#">List Item Name</a></h4>
            <h5>Attribute 1</h5>
            <h6>Attribute 2</h6>
            <p>Vestibulum eu erat vel quam malesuada tempor. Donec et luctus mauris. Cras ornare metus sit amet pellentesque tempus. Phasellus Donec et luctus mauris. Cras ornare...</p>
            <a class="link-with-right-arrow" href="#">Read More</a>
        </div>
        <div class="clear"></div>
    </div><!-- list-box -->
    
	<div class="list-box">
    	<div class="listing-box-thumb col">
        	<a href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/dummy-image.jpg" /></a>
        </div>
        <div class="listing-box-content">
        	<h4><a href="#">List Item Name</a></h4>
            <h5>Attribute 1</h5>
            <h6>Attribute 2</h6>
            <p>Vestibulum eu erat vel quam malesuada tempor. Donec et luctus mauris. Cras ornare metus sit amet pellentesque tempus. Phasellus Donec et luctus mauris. Cras ornare...</p>
            <a class="link-with-right-arrow" href="#">Read More</a>
        </div>
        <div class="clear"></div>
    </div><!-- list-box -->
    
	<div class="list-box">
    	<div class="listing-box-thumb col">
        	<a href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/dummy-image.jpg" /></a>
        </div>
        <div class="listing-box-content">
        	<h4><a href="#">List Item Name</a></h4>
            <h5>Attribute 1</h5>
            <h6>Attribute 2</h6>
            <p>Vestibulum eu erat vel quam malesuada tempor. Donec et luctus mauris. Cras ornare metus sit amet pellentesque tempus. Phasellus Donec et luctus mauris. Cras ornare...</p>
            <a class="link-with-right-arrow" href="#">Read More</a>
        </div>
        <div class="clear"></div>
    </div><!-- list-box -->
    
    
<div class="pagination-box" style="display: none;">
    <div class="pagination">
		<!-- ADD Custom Numbered Pagination code. -->
        <ul>
            <li><span aria-current="page" class="page-numbers current">1</span></li>
            <li><a class="page-numbers" href="#">2</a></li>
            <li><a class="page-numbers" href="#">3</a></li>
            <li><a class="page-numbers" href="#">4</a></li>
            <li><a class="page-numbers" href="#">5</a></li>
            <li><a class="page-numbers" href="#">6</a></li>
	        <li><span class="page-numbers dots">…</span></li>
            <li><a class="page-numbers" href="#">17</a></li>
            <li><a class="next page-numbers" href="#">&gt;</a></li>
        </ul>
	</div>	
</div><!--pagination-box-->
    
<div class="clear"></div>
</div>    <!-- listing-page -->
       
       
       
       
       
       
           
           
        </div>
    </div>
</div>
<?php
get_footer();


<?php get_header(); ?>
<?php
$page_id = get_the_ID();
?>

<div class="banner-section test">
    <?php
    //Featured image
    if (has_post_thumbnail($page_id)) {
        the_post_thumbnail('full');
    }
    ?>

    <div class="container">
        <div class="banner-caption">
            <?php //if (get_field('banner_headline')): ?>
                <h1><?php the_title(); ?><?php //the_field('banner_headline'); ?></h1>
            <?php //endif; ?>
            <?php if (get_field('banner_text')): ?>
                <p><?php the_field('banner_text'); ?></p>
            <?php endif; ?>
        </div>
    </div>
</div>

<?php echo the_breadcrumb(); ?>

<section id="section-sidebar">
    <!--Below hero area content and sidebar-->
    <div class="container">
        <div class="row">


            <div class="col-md-12 col-xs-12">
                <div class="row">
                    <!--Sidebar section-->
                    <div class="col-md-3 col-xs-12 order-md-2">

                        <div class="resourceBox">
                            <?php if (get_field('main_title')): ?>
                                <h6><?php the_field('main_title'); ?></h6>
                            <?php endif; ?>

                            <?php
                            // check if the repeater field has rows of data
                            if (have_rows('resources')):
                                ?>
                                <ul>
                                    <?php
                                    // loop through the rows of data
                                    while (have_rows('resources')) : the_row();
                                        ?>

                                        <?php
                                        $custom_link_arr = get_sub_field('custom_link');
                                        ?>

                                        <li><a href="<?php echo ($custom_link_arr['url'] != '' ? $custom_link_arr['url'] : '#'); ?>"><?php the_sub_field('custom_text'); ?></a></li>

                                <?php endwhile; ?>  
                                </ul>
<?php endif; ?> 
                        </div>  <!-- resourceBox -->

                    </div>

                    <div class="col-md-9 col-xs-12 order-md-1">
                        <?php /*<h1><?php the_title(); ?></h1>*/?>
                        <?php if (get_field('banner_headline')): ?>
                            <h1><?php the_field('banner_headline'); ?></h1>
                        <?php endif; ?>

                        <!--Subtitle-->
                        <?php if (get_field('subtitle')): ?>
                            <h5><?php the_field('subtitle'); ?></h5>
                        <?php endif; ?>
                        <?php /* ?><h6>Author, other metadata. </h6><?php */ ?>
                        <?php /* ?>
                          <!--Author section-->
                          <?php if (get_field('author')): ?>
                          <h6 style="margin-bottom: 0;"><?php the_field('author'); ?></h6>
                          <?php endif; ?>
                          <?php */ ?>
                    </div>

                </div>
            </div>



            <div class="col-md-12 col-xs-12">
                <!--Subcopy text-->
                <?php if (get_field('text_area')): ?>
                    <p><?php the_field('text_area'); ?></p>
<?php endif; ?>

            </div>  

        </div>         
    </div>   
</section>


<div class="page-container">
    <!--Main content-->
    <div class="container">
        <div class="main-content">  
            <?php
            if (have_posts()) {
                while (have_posts()) : the_post();
                    ?>            
                    <?php the_content(); ?>                
                <?php endwhile; ?>
<?php } ?>
        </div>
    </div>
</div>

<?php
get_footer();

<?php

/*
  Element Description: VC AllInOne Box
  https://kb.wpbakery.com/docs/inner-api/vc_map/
  http://www.wpelixir.com/how-to-create-new-element-in-visual-composer/
 */

// Element Class 
class vcAllInOneImgGradBox extends WPBakeryShortCode {

    // Element Init
    function __construct() {
        add_action('init', array($this,'vc_all_in_ones_imggrad_mapping'));
        add_shortcode('vc_all_in_imggrad_ones', array($this,'vc_all_in_ones_imggrad_html'));
    }

    // Element Mapping
    // Element Mapping
    public function vc_all_in_ones_imggrad_mapping() {

        // Stop all if VC is not enabled
        if (!defined('WPB_VC_VERSION')) {
            return;
        }

        // Map the block with vc_map()
        vc_map(
                array(
                    'name' => __('Text/Card with Image Gradient', 'all_in_one_imggrad-domain'),
                    'base' => 'vc_all_in_imggrad_ones',
                    'description' => __('Title, Content, Image, Color, Link etc. Box', 'all_in_one_imggrad-domain'),
                    'category' => __('fusionSpan', 'all_in_one_imggrad-domain'),
                    'icon' => get_stylesheet_directory() . '/images/card-icon.png',
                    'params' => array(
                        array(
                            'type' => 'attach_image',
                            'holder' => 'img',
                            'class' => 'attach_image_icon',
                            'heading' => __('Card Image', 'all_in_one_imggrad-domain'),
                            'param_name' => 'all_in_one_image',
                            'value' => ''
                        ),
                        array(
                            "type" => "textfield",
                            "holder" => "h3",
                            "class" => "",
                            "heading" => __("Title", "all_in_one_imggrad-domain"),
                            "param_name" => "all_in_one_title",
                            "value" => __("", "all_in_one_imggrad-domain"),
                            "description" => __("Add Title here.", "all_in_one_imggrad-domain")
                        ),
                        array(
                            "type" => "textarea_html",
                            "holder" => "div",
                            "class" => "",
                            "heading" => __("Description", "all_in_one_imggrad-domain"),
                            "param_name" => "content",//Important: Only one textarea_html param per content element allowed and it should have "content" as a "param_name"
                            "value" => __("<p>I am test text block. Click edit button to change this text.</p>", "all_in_one_imggrad-domain"),
                            "description" => __("Enter your content.", "all_in_one_imggrad-domain")
                        ),
                        array(
                            'type' => 'vc_link',
                            'class' => 'text-member-links',
                            'holder' => 'a',
                            'heading' => __('Link URL', 'all_in_one_imggrad-domain'),
                            'param_name' => 'all_in_one_links',
                            'description' => __('Add link here', 'all_in_one_imggrad-domain')
                        )
                    )//params
                )//main array
        );
    }

    // Element HTML
    public function vc_all_in_ones_imggrad_html($atts, $content = null) {

        // Params extraction
        extract(
            shortcode_atts(
                array(
                    'all_in_one_image' => '',
                    'all_in_one_title' => '',
                    'all_in_one_links' => '',
                ), $atts
            )
        );

        $url = ($all_in_one_links == '||') ? '' : $all_in_one_links;
        $url = vc_build_link($url);
        $a_link = ($url['url'] == '') ? '' : 'href="'.trim($url['url']).'"';
        $a_title = ($url['title'] == '') ? '' : 'title="' . $url['title'] . '"';
        $a_target = ($url['target'] == '') ? '' : 'target="'.trim($url['target']).'"';
        
        $mbhtml = '<div class="module-with-image-gradient">
                        <div class="three-col-module">
                            <div class="module-img-section">';
        
                            if ($all_in_one_image != "") {

                                $image_ids = explode(',', $all_in_one_image);
                                foreach ($image_ids as $image_id) {
                                    $images = wp_get_attachment_image_src($image_id, 'full');

                                    if ($a_link != '') {
                                        $mbhtml .= '<a ' . $a_link . ' '.$a_target.'><img src="' . $images[0] . '" alt="' . $all_in_one_title . '" title="'.get_the_title($image_id).'"></a>';
                                    } else {
                                        $mbhtml .= '<img src="' . $images[0] . '" alt="' . $all_in_one_title . '" title="'.get_the_title($image_id).'">';
                                    }

                                    $images++;
                                }
                            }
                                
                            $mbhtml .= '<div class="module-title-txt">';
                                    
                                        if($url['url'] != ''){
                                            $mbhtml .= '<h3><a '.$a_link.' '.$a_target.'>'.$all_in_one_title.'</a></h3>';
                                        }else {
                                            $mbhtml .= '<h3>'.$all_in_one_title.'</h3>';
                                        }
                                            
                                        $mbhtml .= '<div class="para-txt"><p>' . $content . '</p></div>';
                                        if($url['url'] != ''){
                                            $mbhtml .= '<a class="readmore" ' . $a_link . ''.$a_target.'>'.($url['url'] != '' ? 'Read More' : '').'</a>';
                                        }
                                        
                                $mbhtml .= '</div>
                            </div>
                        </div>
                    </div>';
        
        return $mbhtml;
    }
}

// End Element Class
// Element Class Init
new vcAllInOneImgGradBox();

<?php

/*
  Element Description: VC AllInOne Box
  https://kb.wpbakery.com/docs/inner-api/vc_map/
  http://www.wpelixir.com/how-to-create-new-element-in-visual-composer/
 */

// Element Class 
class vcAllInOneBox extends WPBakeryShortCode {

    // Element Init
    function __construct() {
        add_action('init', array($this,'vc_all_in_ones_mapping'));
        add_shortcode('vc_all_in_ones', array($this,'vc_all_in_ones_html'));
    }

    // Element Mapping
    // Element Mapping
    public function vc_all_in_ones_mapping() {

        // Stop all if VC is not enabled
        if (!defined('WPB_VC_VERSION')) {
            return;
        }

        // Map the block with vc_map()
        vc_map(
                array(
                    'name' => __('Text/Card Module', 'all_in_one-domain'),
                    'base' => 'vc_all_in_ones',
                    'description' => __('Title, Content, Image, Color, Link etc. Box', 'all_in_one-domain'),
                    'category' => __('fusionSpan', 'all_in_one-domain'),
                    'icon' => get_stylesheet_directory() . '/images/card-icon.png',
                    'params' => array(
                        array(
                            'type' => 'attach_image',
                            'holder' => 'img',
                            'class' => 'attach_image_icon',
                            'heading' => __('Card Image', 'all_in_one-domain'),
                            'param_name' => 'all_in_one_image',
                            'value' => ''
                        ),
                        array(
                            "type" => "textfield",
                            "holder" => "h3",
                            "class" => "",
                            "heading" => __("Title", "all_in_one-domain"),
                            "param_name" => "all_in_one_title",
                            "value" => __("", "all_in_one-domain"),
                            "description" => __("Add Title here.", "all_in_one-domain")
                        ),
                        array(
                            "type" => "textfield",
                            "holder" => "h3",
                            "class" => "",
                            "heading" => __("Subtitle", "all_in_one-domain"),
                            "param_name" => "all_in_one_subtitle",
                            "value" => __("", "all_in_one-domain"),
                            "description" => __("Add Subtitle here.", "all_in_one-domain")
                        ),
                        array(
                            "type" => "textarea_html",
                            "holder" => "div",
                            "class" => "",
                            "heading" => __("Description", "all_in_one-domain"),
                            "param_name" => "content",//Important: Only one textarea_html param per content element allowed and it should have "content" as a "param_name"
                            "value" => __("<p>I am test text block. Click edit button to change this text.</p>", "all_in_one-domain"),
                            "description" => __("Enter your content.", "all_in_one-domain")
                        ),
                        array(
                            'type' => 'vc_link',
                            'class' => 'text-member-links',
                            'holder' => 'a',
                            'heading' => __('Link URL', 'all_in_one-domain'),
                            'param_name' => 'all_in_one_links',
                            'description' => __('Add link here', 'all_in_one-domain')
                        )
                        /*array(
                            'type' => 'dropdown',
                            'heading' => __( 'Image Orientation',  "all_in_one-domain" ),
                            'param_name' => 'image_orientation',
                            'value' => array(
                              __( 'Landscape',  "all_in_one-domain"  ) => 'landscape',
                              __( 'Portrait',  "all_in_one-domain"  ) => 'portrait'
                            ),
                            'description' => __('Image Oriention to change the view of the module.<br/><br/>', 'all_in_one-domain')
                        )*/
                    )//params
                )//main array
        );
    }

    // Element HTML
    public function vc_all_in_ones_html($atts, $content = null) {

        // Params extraction
        extract(
            shortcode_atts(
                array(
                    'all_in_one_image' => '',
                    'all_in_one_title' => '',
                    'all_in_one_subtitle' => '',
                    'all_in_one_links' => ''
                    //'image_orientation' => ''
                ), $atts
            )
        );

        $url = ($all_in_one_links == '||') ? '' : $all_in_one_links;
        $url = vc_build_link($url);
        $a_link = $url['url'];
        $a_title = ($url['title'] == '') ? '' : 'title="' . $url['title'] . '"';
        $a_target = ($url['target'] == '') ? '' : 'target="'.trim($url['target']).'"';
		$button = $a_link ? '<a class="link-with-right-arrow" href="' . $a_link . '" ' . $a_title . ' ' . $a_target . '>' . $url['title'] . '</a>' : '';

        $mbhtml = '<div class="top-image-card '.($a_link != '' ? 'top-image-card-hover' : '').'">';

        if ($all_in_one_image != "") {
            $mbhtml .= '<div class="module-image-wrap">';

            $image_ids = explode(',', $all_in_one_image);
            foreach ($image_ids as $image_id) {
                $images = wp_get_attachment_image_src($image_id, 'full');

                if ($a_link != '') {
                    $mbhtml .= '<a href="' . $a_link . '"'.$a_target.'><img src="' . $images[0] . '" alt="' . $all_in_one_title . '" title="'.get_the_title($image_id).'"></a>';
                } else {
                    $mbhtml .= '<img src="' . $images[0] . '" alt="' . $all_in_one_title . '" title="'.get_the_title($image_id).'">';
                }

                $images++;
            }


            $mbhtml .= '</div>';
        }//all_in_one_image
         $mbhtml .= '<div class="top-module-content" >';
        //Title
        if ($all_in_one_title != "") {
            if ($a_link != '') {
                $mbhtml .= '<div class="module-title"><h5><a href="' . $a_link . '"'.$a_target.'>' . $all_in_one_title . '</a></h5></div>';
            } else {
                $mbhtml .= '<div class="module-title"><h5>' . $all_in_one_title . '</h5></div>';
            }
        }

	
        //SubTitle
        if ($all_in_one_subtitle != "") {
            $mbhtml .= '<div class="module-title"><h6>' . $all_in_one_subtitle . '</h6></div>';
        }

        $mbhtml .= '<div class="module-card">
                      <div class="module-text">' . $content . '</div>
					    '.$button.'
					   </div>
                    <div class="clear"></div>
                  </div>';
		 $mbhtml .= '</div>';
        $mbhtml .= '';
		
        return $mbhtml;
    }

    public function is_html($string) {
        // Check if string contains any html tags.
        return preg_match('/<\s?[^\>]*\/?\s?>/i', $string);
        //return $string != strip_tags($string) ? true:false;
    }

    public function Truncate($text, $length, $suffix = '&hellip;') {

        $i = 0;

        if ($this->is_html($text) == 1) {
            $output = $text;
        } else {

            // output without closing tags
            $output = substr($text, 0, $length = min(strlen($text), $length + $i));

            // Add suffix if needed
            if (strlen($text) > $length) {
                $one .= $suffix;
            }
            // Re-attach tags
            $output .= $one;

            //added to remove  unnecessary closure
            $output = str_replace('</!-->', '', $output);
        }

        return $output;
    }

}

// End Element Class
// Element Class Init
new vcAllInOneBox();

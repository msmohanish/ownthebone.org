<?php

/*
  Element Description: VC AllInOne Box
  https://kb.wpbakery.com/docs/inner-api/vc_map/
  http://www.wpelixir.com/how-to-create-new-element-in-visual-composer/
 */

// Element Class 
class vcAllInOneBlueBox extends WPBakeryShortCode {

    // Element Init
    function __construct() {
        add_action('init', array($this, 'vc_all_in_ones_blue_mapping'));
        add_shortcode('vc_all_in_blueones', array($this, 'vc_all_in_green_ones_html'));
    }

    // Element Mapping
    // Element Mapping
    public function vc_all_in_ones_blue_mapping() {

        // Stop all if VC is not enabled
        if (!defined('WPB_VC_VERSION')) {
            return;
        }

        // Map the block with vc_map()
        vc_map(
                array(
                    'name' => __('Full Width Call to Action', 'all_in_green_one-domain'),
                    'base' => 'vc_all_in_blueones',
                    'description' => __('WYSIWYG, Image', 'all_in_green_one-domain'),
                    'category' => __('fusionSpan', 'all_in_green_one-domain'),
                    'icon' => get_stylesheet_directory() . '/images/card-icon.png',
                    'params' => array(
                        array(
                            'type' => 'attach_image',
                            'holder' => 'img',
                            'class' => 'attach_image_icon',
                            'heading' => __('Background Image', 'all_in_green_one-domain'),
                            'param_name' => 'all_in_one_blue_image',
                            'value' => ''
                        ),
                        array(
                            "type" => "textarea_html",
                            "holder" => "div",
                            "class" => "",
                            "heading" => __("Description", "all_in_green_one-domain"),
                            "param_name" => "content",//Important: Only one textarea_html param per content element allowed and it should have "content" as a "param_name"
                            "value" => __("<p>I am test text block. Click edit button to change this text.</p>", "all_in_green_one-domain"),
                            "description" => __("Enter your content.", "all_in_green_one-domain")
                        ),
                        array(
                            'type' => 'vc_link',
                            'class' => 'text-member-links',
                            'holder' => 'a',
                            'heading' => __('Link URL 1', 'all_in_green_one-domain'),
                            'param_name' => 'all_in_one_links',
                            'description' => __('Add link 1 here', 'all_in_green_one-domain')
                        ),
                        array(
                            'type' => 'vc_link',
                            'class' => 'text-member-links',
                            'holder' => 'a',
                            'heading' => __('Link URL 2', 'all_in_green_one-domain'),
                            'param_name' => 'all_in_two_links',
                            'description' => __('Add link 2 here', 'all_in_green_one-domain')
                        ),
                        array(
                            'type' => 'dropdown',
                            'heading' => __('Select Button Alignment', "all_in_green_one-domain"),
                            'param_name' => 'button_align',
                            'value' => array(
                                __('Left', "all_in_one-domain") => 'button-left',
                                __('Right', "all_in_one-domain") => 'button-right',
                            ),
                            'std' => 'no-class',// Your default value
                            "description" => __("Select Button Alignment to be applied", "all_in_green_one-domain")
                        ),
                    )//params
                )//main array
        );
    }

    // Element HTML
    // Element HTML
    public function vc_all_in_green_ones_html($atts, $content = null) {

        // Params extraction
        extract(
            shortcode_atts(
                array(
                    'all_in_one_blue_image' => '',
                    'all_in_one_links' => '',
                    'all_in_two_links' => '',
                    'button_align' => ''
                ), $atts
            )
        );
        
        $url1 = ($all_in_one_links == '||') ? '' : $all_in_one_links;
        $url1 = vc_build_link($url1);
        $a_link1 = $url1['url'];
        $a_title1 = ($url1['title'] == '') ? '' : 'title="' . $url1['title'] . '"';
        $a_target1 = ($url1['target'] == '') ? '' : 'target="' . $url1['target'] . '"';
        $button1 = $a_link1 ? '<a class="gold-button-with-arrow" href="' . $a_link1 . '" ' . $a_title1 . ' ' . $a_target1 . '>' . $url1['title'] . '</a>' : '';
        
        $url2 = ($all_in_two_links == '||') ? '' : $all_in_two_links;
        $url2 = vc_build_link($url2);
        $a_link2 = $url2['url'];
        $a_title2 = ($url2['title'] == '') ? '' : 'title="' . $url2['title'] . '"';
        $a_target2 = ($url2['target'] == '') ? '' : 'target="' . $url2['target'] . '"';
        $button2 = $a_link2 ? '<a class="gold-button-with-arrow" href="' . $a_link2 . '" ' . $a_title2 . ' ' . $a_target2 . '>' . $url2['title'] . '</a>' : '';

        /* ========= Html Start here======== */
        $mbhtml = '<div class="all-in-one green-module-card">';

        /* ========= Html Start here======== */
        if ($all_in_one_blue_image != "") {
            $image_ids = explode(',', $all_in_one_blue_image);
            foreach ($image_ids as $image_id) {
                $images = wp_get_attachment_image_src($image_id, 'full');

                $mbhtml .= '<div class="member-image-wrap"><img class="member-benefit-image" src="' . $images[0] . '" /></div>';

                $images++;
            }
        }//all_in_one_image

        //Content
        $mbhtml .= '<div class="member-card">';
        
        $sun_icon = '<img src="'.get_stylesheet_directory_uri() .'/images/sun-icon.png">';
        
        $content = str_replace('[sun-icon]', $sun_icon, $content);

        if ($button_align != "" && $button_align == 'button-left') {
            $mbhtml .= '<div class="member-benefit-text"><div class="button-left">'.$button1.$button2.'</div><h3>' . $content . '</h3></div>';
        }
        if ($button_align != "" && $button_align == 'button-right') {
            $mbhtml .= '<div class="member-benefit-text"><h3>' . $content . '</h3><div class="button-right">'.$button1.$button2.'</div></div>';
        }

        $mbhtml .= '</div></div>'; //member-card body

        return $mbhtml;
    }
    
}

// End Element Class
// Element Class Init
new vcAllInOneBlueBox();

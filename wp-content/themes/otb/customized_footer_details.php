<?php
function aoa_contact_details($wp_customize){
    //  =============================
    //  = Section Header Logo     =
    //  =============================
    
    $wp_customize->add_section('header_logo', array(
        'title'      => __( 'Header Site Logo' ),
        'description' => '',
        'priority'   => 90,
        )
    ); 

    //  =============================
    //  = Header Logo Image Upload   =
    //  =============================
    $wp_customize->add_setting('header_logo_upload', array(
        'default'           => '',
        'capability'        => 'edit_theme_options',
        'type'           => 'theme_mod',
 
    ));
 
    $wp_customize->add_control( new WP_Customize_Image_Control($wp_customize, 'header_logo_upload', array(
        'label'    => __('Organization logo'),
        'section'  => 'header_logo',
    )));
 
    //  =============================
    //  = Header Call to action button   =
    //  =============================
    
    $wp_customize->add_section('header_call_to_action', array(
        'title'      => __( 'Header Contact Us Button' ),
        'description' => '',
        'priority'   => 95,
        )
    ); 

    //  ===================================
    //  = Call to action button text =
    //  ===================================
    $wp_customize->add_setting('call_action_text', array(
        'default'        => '',
        'capability'     => 'edit_theme_options',
        'type' => 'theme_mod',
 
    ));
 
    $wp_customize->add_control('call_action_text', array(
        //'type'     => 'textarea',
        'priority' => 10,
        'label'      => __('Button Text'),
        'section'    => 'header_call_to_action',
    ));

    //  ===================================
    //  = Call to action button link =
    //  ===================================
    $wp_customize->add_setting('call_action_link', array(
        'default'        => '',
        'capability'     => 'edit_theme_options',
        'type' => 'theme_mod',
 
    ));
 
    $wp_customize->add_control('call_action_link', array(
        'priority' => 10,
        'label'      => __('Button Link'),
        'section'    => 'header_call_to_action',
    ));
    
	
    //  =============================
    //  = Social Channels Section   =
    //  =============================
    
    $wp_customize->add_section('social_channels', array(
        'title'      => __( 'Social Channels' ),
        'description' => '',
        'priority'   => 220,
        )
    ); 
    
    //  ===============================
    //  =Social Channels Text Input 1 facebook =
    //  ===============================
    $wp_customize->add_setting('social_facebook', array(
        'default'        => '',
        'capability'     => 'edit_theme_options',
        'type' => 'theme_mod',
 
    ));
 
    $wp_customize->add_control('social_facebook', array(
        'type' => 'url',
        'priority' => 10,
        'label'      => __('Facebook'),
        'section'    => 'social_channels',
    ));
    
    //  ===============================
    //  =Social Channels Text Input 2 twitter =
    //  ===============================
    $wp_customize->add_setting('social_twitter', array(
        'default'        => '',
        'capability'     => 'edit_theme_options',
        'type' => 'theme_mod',
 
    ));
 
    $wp_customize->add_control('social_twitter', array(
        'type' => 'url',
        'priority' => 10,
        'label'      => __('Twitter'),
        'section'    => 'social_channels',
    ));

    //  ===============================
    //  =Social Channels Text Input 3 linkedIn =
    //  ===============================
    $wp_customize->add_setting('social_linkedIn', array(
        'default'        => '',
        'capability'     => 'edit_theme_options',
        'type' => 'theme_mod',
 
    ));
 
    $wp_customize->add_control('social_linkedIn', array(
        'type' => 'url',
        'priority' => 10,
        'label'      => __('LinkedIn'),
        'section'    => 'social_channels',
    ));
    
    //  ===============================
    //  =Social Channels Text Input 4 youTube =
    //  ===============================
    $wp_customize->add_setting('social_youTube', array(
        'default'        => '',
        'capability'     => 'edit_theme_options',
        'type' => 'theme_mod',
 
    ));
 
    $wp_customize->add_control('social_youTube', array(
        'type' => 'url',
        'priority' => 10,
        'label'      => __('YouTube'),
        'section'    => 'social_channels',
    ));
	
	//  ===============================
    //  =Social Channels Text Input 4 youTube =
    //  ===============================
    $wp_customize->add_setting('social_flickr', array(
        'default'        => '',
        'capability'     => 'edit_theme_options',
        'type' => 'theme_mod',
 
    ));
 
    $wp_customize->add_control('social_flickr', array(
        'type' => 'url',
        'priority' => 10,
        'label'      => __('Flickr'),
        'section'    => 'social_channels',
    ));
	
    //  ===============================
    //  =Social Channels Text Input 5 instagram =
    //  ===============================
    $wp_customize->add_setting('social_instagram', array(
        'default'        => '',
        'capability'     => 'edit_theme_options',
        'type' => 'theme_mod',
 
    ));
 
    $wp_customize->add_control('social_instagram', array(
        'type' => 'url',
        'priority' => 10,
        'label'      => __('Instagram'),
        'section'    => 'social_channels',
    ));
    
    
    
    //  =============================
    //  = Contact Information Section   =
    //  =============================
    
    $wp_customize->add_section('contact_information', array(
        'title'      => __( 'Footer Section' ),
        'description' => '',
        'priority'   => 240,
        )
    ); 

    //  =============================
    //  = Footer Logo Image Upload   =
    //  =============================

    // $wp_customize->add_setting('footer_logo_upload', array(
    //     'default'           => '',
    //     'capability'        => 'edit_theme_options',
    //     'type'           => 'theme_mod',
 
    // ));
 
    // $wp_customize->add_control( new WP_Customize_Image_Control($wp_customize, 'footer_logo_upload', array(
    //     'label'    => __('Organization logo'),
    //     'section'  => 'contact_information',
    // )));
    
    
    //  ===================================
    //  =Footer Logo
    //  ===================================
    $wp_customize->add_setting('footer_logo', array(
        'default'           => '',
        'capability'        => 'edit_theme_options',
        'type'           => 'theme_mod',
 
    ));
 
    $wp_customize->add_control( new WP_Customize_Image_Control($wp_customize, 'footer_logo', array(
        'label'    => __('Upload Footer logo'),
        'section'  => 'contact_information',
    )));
    
    
    //Footer login link
    $wp_customize->add_setting('footer_login_link', array(
        'default'        => '',
        'capability'     => 'edit_theme_options',
        'type' => 'theme_mod',
 
    ));
 
    $wp_customize->add_control('footer_login_link', array(
        //'type' => 'email',
        'priority' => 10,
        'label'      => __('Footer Register Link'),
        'section'    => 'contact_information',
    ));
    
    //Footer Contact Us link
    $wp_customize->add_setting('footer_contactus_link', array(
        'default'        => '',
        'capability'     => 'edit_theme_options',
        'type' => 'theme_mod',
 
    ));
 
    $wp_customize->add_control('footer_contactus_link', array(
        //'type' => 'email',
        'priority' => 10,
        'label'      => __('Footer Login Link'),
        'section'    => 'contact_information',
    ));
    
    
    //  ===================================
    //  =Contact Information Privacy Policy Text  =
    //  ===================================
    $wp_customize->add_setting('policy_text', array(
        'default'        => '',
        'capability'     => 'edit_theme_options',
        'type' => 'theme_mod',
 
    ));
 
    $wp_customize->add_control('policy_text', array(
        //'type' => 'email',
        'priority' => 10,
        'label'      => __('Terms Of Service Text', 'twentytwenty'),
        'section'    => 'contact_information',
    ));

    //  ===================================
    //  =Contact Information Privacy Policy Link  =
    //  ===================================
    $wp_customize->add_setting('policy_link', array(
        'default'        => '',
        'capability'     => 'edit_theme_options',
        'type' => 'theme_mod',
 
    ));
 
    $wp_customize->add_control('policy_link', array(
        //'type' => 'email',
        'priority' => 10,
        'label'      => __('Terms Of Service Link', 'twentytwenty'),
        'section'    => 'contact_information',
    ));
	
	//  ===================================
    //  =Contact Information Refund Policy text  =
    //  ===================================
    $wp_customize->add_setting('refund_policy_text', array(
        'default'        => '',
        'capability'     => 'edit_theme_options',
        'type' => 'theme_mod',
 
    ));
 
    $wp_customize->add_control('refund_policy_text', array(
        //'type' => 'email',
        'priority' => 10,
        'label'      => __('Refund Policy Text', 'twentytwenty'),
        'section'    => 'contact_information',
    ));
	
	//  ===================================
    //  =Contact Information Refund Policy Link  =
    //  ===================================
    $wp_customize->add_setting('refund_policy_link', array(
        'default'        => '',
        'capability'     => 'edit_theme_options',
        'type' => 'theme_mod',
 
    ));
 
    $wp_customize->add_control('refund_policy_link', array(
        //'type' => 'email',
        'priority' => 10,
        'label'      => __('Refund Policy Link', 'twentytwenty'),
        'section'    => 'contact_information',
    ));
    
    //  ===================================
    //  =Contact Information Text Input 3 =
    //  ===================================
    $wp_customize->add_setting('copyright_text', array(
        'default'        => '',
        'capability'     => 'edit_theme_options',
        'type' => 'theme_mod',
 
    ));
 
    $wp_customize->add_control('copyright_text', array(
        //'type' => 'email',
        'priority' => 10,
        'label'      => __('Footer Copyright Text'),
        'section'    => 'contact_information',
    ));

}

add_action('customize_register', 'aoa_contact_details');



// Before VC Init
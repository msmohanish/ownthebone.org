<?php
get_header();

//get posts = post_type= events
$args = array(
    'post_type' => 'events',
    'post_status' => 'publish',
    'posts_per_page' => -1,
    'orderby' => 'date',
    'order' => 'DESC'
);

$e_posts = get_posts($args);

$event_arr = [];
$today = time();

//first sort the event by date
if(!empty($e_posts)){

    foreach($e_posts as $k => $v){

        $event_start_date = get_field('event_date', $v->ID);
        $timestamp = strtotime($event_start_date);

        //it should be greater than today
        if($timestamp > $today){
            $event_arr[$k]['ID'] = $v->ID;
            $event_arr[$k]['event_sort_date'] = $timestamp;
        }
    }

    //sort array
    @usort($event_arr, function($a, $b) {  //descending
        return $a["event_sort_date"] > $b["event_sort_date"]; 
    });
}
?>

<div class="page-container all-events-page-style">
    <!--Main content-->
    <div class="container">
        <div class="main-content">  
            
            <h1><?php the_title(); ?></h1>
            
            <?php
                //show events
                if(!empty($event_arr)){

                    foreach($event_arr as $ek => $ev){

                        $post_ID = $ev['ID'];

                        $content = get_post_field('post_content', $post_ID);
                        $content = mb_strimwidth($content, 0, 160, '... ');
                        $content = strip_tags($content);

                        //event date
                        $event_date = get_field('event_date', $post_ID);

                        $year = date("Y", strtotime($event_date));
                        $date = date("d", strtotime($event_date));
                        $month = date("M", strtotime($event_date));

                        echo $event_html = '<div class="featured-events">
                                    <div class="events-date"><strong>'.$date.'</strong><strong>'.$month.'</strong><span>'.$year.'</span></div>
                                <div class="featured-events-content">
                                    <a href="#"><h5>'.get_the_title($post_ID).'</h5></a>
                                    <p>'.$content.'</p>
                                </div>
                                <div class="clear"></div>
                            </div>';
                    }
                }
            ?>
        </div>
    </div>
</div>

<?php

get_footer();
<?php
/**
 * The template for displaying the footer
 */
?>



<footer class="site-footer">
    <div class="container">
        <div class="row">

            <div class="col-xl-4 col-lg-3 col-md-12">
                <div class="footer-logo">
                    <a href="<?php echo get_home_url(); ?>"><img src="<?php echo get_theme_mod('footer_logo'); ?>"  alt="AOA" /></a>
                </div>
            </div>

            <div class="footer-navigation col-xl-2 col-lg-2 col-md-3">
                <?php wp_nav_menu(array('theme_location' => 'footer1')); ?>
            </div>

            <div class="footer-navigation col-xl-2 col-lg-2 col-md-3">
                <?php wp_nav_menu(array('theme_location' => 'footer2')); ?>
            </div>

            <div class="footer-navigation col-xl-2 col-lg-2 col-md-3">
                <?php wp_nav_menu(array('theme_location' => 'footer3')); ?>
            </div>

            <div class="col-xl-2 col-lg-3 col-md-3 footer-button-res">
                <a class="button-with-arrow" href="<?php echo get_theme_mod('footer_login_link'); ?>">Contact Us</a><br />
                <?php if (!is_user_logged_in()) { ?>
                    <a class="button-with-arrow" href="/wp-login.php?action=wp-saml-auth&redirect_to=/">Login</a>
                <?php } else { ?>
                    <a class="button-with-arrow" href="<?php echo wp_logout_url(site_url()); ?>">Logout</a>
                <?php } ?>
            </div> 

            <div class="clear"></div>
        </div><!-- row -->


        <div class="copyright-area">        
            <div class="copyright">
                <?php echo str_replace('CYEAR', date('Y'), get_theme_mod('copyright_text')); ?>  |  
                <a href="<?php $content_mod = get_theme_mod('policy_link'); echo $content_mod; ?>">
                    <?php $content_mod = get_theme_mod('policy_text'); echo $content_mod; ?></a> 



                <?php if ($content_mod = get_theme_mod('refund_policy_link')): ?>  | 
                    <a href="<?php $content_mod = get_theme_mod('refund_policy_link'); echo $content_mod; ?>"><?php $content_mod = get_theme_mod('refund_policy_text'); echo $content_mod; ?></a>
                <?php endif; ?>


            </div>
            <div class="clear"></div>
        </div><!-- copyright-area -->

    </div>
    <?php
    $remote_site = get_field('remote_site', 'option');
    $sf_logout_url = get_field('single_logout_service_url', 'option');
    ?>
    <input type="hidden" id="remote_site" value="<?php echo $remote_site; ?>" />
    
    <input type="hidden" id="sf_logout_url" value="<?php echo $sf_logout_url; ?>" />
    <iframe id="sfframe" src="" width="0" height="0"></iframe>
</footer>

<?php wp_footer(); ?>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/custom.js?v=<?php echo time(); ?>"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/bootstrap.min.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/owl.carousel.js"></script>
<link href="<?php echo get_stylesheet_directory_uri(); ?>/css/owl.carousel.css" rel="stylesheet" type="text/css">

</body>
</html>

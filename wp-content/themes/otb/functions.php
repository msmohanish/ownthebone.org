<?php
/** Start the engine */
require_once( get_template_directory() . '/lib/init.php' );
/** Child theme (do not remove) */
define( 'CHILD_THEME_NAME', 'OTB theme' );
//define( 'CHILD_THEME_URL', 'http://www.studiopress.com/themes/fusionspan' );

include('customized_footer_details.php');
//Include custom VC module
include( "vc-elements/all-in-one-textcard-module.php" );
include( "vc-elements/all-in-one-green-module.php" );
include( "vc-elements/text-card-with-image-gradient.php" );

/** Add Viewport meta tag for mobile browsers */
add_action('genesis_meta', 'aoa_viewport_meta_tag');

function aoa_viewport_meta_tag() {
    echo '<meta name="viewport" content="width=device-width, initial-scale=1.0"/>';
}

/**
 * Register navigation menus uses wp_nav_menu in five places.
 */
function genesis_menus_menus() {
    $locations = array(
        'primary' => __('Primary Menu', 'genesis-menus'),
        'topmenu' => __('Top Bar Menu', 'genesis-menus'),
        'footer1' => __('Footer Menu 1', 'genesis-menus'),
		'footer2' => __('Footer Menu 2', 'genesis-menus'),
		'footer3' => __('Footer Menu 3', 'genesis-menus'),	
    );
    register_nav_menus($locations);
}

add_action('init', 'genesis_menus_menus');

/** Unregister default sidebars genesis theme */
unregister_sidebar( 'header-right' );
unregister_sidebar( 'secondary-sidebar' );
unregister_sidebar( 'sidebar' );
unregister_sidebar( 'sidebar-alt' );


function aoa_register_sidebars() {
	register_sidebar( array( // Start a series of sidebars to register
		'id' => 'sidebar-1', // Make an ID
		'name' => 'Post Sidebar', // Name it
		'description' => 'Take it on the side...', // Dumb description for the admin side
		'before_widget' => '<div class="widget-area"><aside class="widget">', // What to display before each widget
		'after_widget' => '</aside></div>', // What to display following each widget
		'before_title' => '<h2 class="widget-title">', // What to display before each widget's title
		'after_title' => '</h2>', // What to display following each widget's title
		'empty_title' => '', // What to display in the case of no title defined for a widget
	) );
}
// adding sidebars to Wordpress (these are created in functions.php)
add_action( 'widgets_init', 'aoa_register_sidebars' );
 

function cc_mime_types($mimes) {
 $mimes['svg'] = 'image/svg+xml';
 return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');


function pagenavi($wp_query) 
{
    //global $wp_query;
    $big = 999999999;
    if($wp_query->max_num_pages >5)
    {
      $true=true;
    }else{
      $true=false;
    }
    
    $page_format = paginate_links( array(
        'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
        'format' => '?paged=%#%',
        'current' => max( 1, get_query_var('paged') ),
        'total' => $wp_query->max_num_pages,
        'next_text' => __( '&nbsp;' ),
        'prev_text' => __( '&nbsp;' ),
        //'end_size'     => 5,
        'mid_size'     => 1,
        //'prev_next' => $true,
        'type'  => 'array'
    ) );

    if( is_array($page_format) ) 
    {
                $paged = ( get_query_var('paged') == 0 ) ? 1 : get_query_var('paged');
                echo '<div><ul>';
                //echo '<li><span>'. $paged . ' of ' . $wp_query->max_num_pages .'</span></li>';
                foreach ( $page_format as $page ) {
                        echo "<li>$page</li>";
                }
               echo '</ul></div>';
    }

    ?>
    <?php
}
//================================================
function pagenation_next_prev_mid_size($wp_query) 
{
    /*We can set pre next with middle size and display next prev after certian page limits reached.*/
    //global $wp_query;
    $big = 999999999;
    if($wp_query->max_num_pages >5)
    {
      $true=true;
    }else{
      $true=false;
    }
    
    $page_format = paginate_links( array(
        'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
        'format' => '?paged=%#%',
        'current' => max( 1, get_query_var('paged') ),
        'total' => $wp_query->max_num_pages,
        'next_text' => __( '&gt;' ),
        'prev_text' => __( '&lt;' ),
        //'end_size'     => 5,
        'mid_size'     => 10,
        'prev_next' => $true,
        'type'  => 'array'
    ) );
    if( is_array($page_format) ) {
                $paged = ( get_query_var('paged') == 0 ) ? 1 : get_query_var('paged');
                echo '<div><ul>';
                //echo '<li><span>'. $paged . ' of ' . $wp_query->max_num_pages .'</span></li>';
                foreach ( $page_format as $page ) {
                        echo "<li>$page</li>";
                }
               echo '</ul></div>';
    }

    ?>
    <?php
}

//login Page Css
function media_login_stylesheet() {
    wp_enqueue_style( 'custom-login', get_stylesheet_directory_uri() . '/css/wp-login.css' );
}
add_action( 'login_enqueue_scripts', 'media_login_stylesheet' );

function media_login_logo_url() {
    return get_bloginfo( 'url' );
}
add_filter( 'login_headerurl', 'media_login_logo_url' );

// customize admin bar css
function override_admin_bar_css() { 
   if ( is_admin_bar_showing() ) { ?>
      <style type="text/css">
         /* add your style here */
         .site-header {
            margin-top: 31px !important;
        } 
      </style>
   <?php }
}
// on frontend area
add_action( 'wp_head', 'override_admin_bar_css' );
 

/*Breadcrumbs*/
function the_breadcrumb($atts='') {
    
    if ( !is_search() ) {
        
        echo '<div class="breadcrumbs"><div class="container">';

        global $post, $wp_query;
        $post_id = get_the_ID();

        $page_title = $wp_query->post->post_title;

        $ancestors = get_post_ancestors($post_id);

        echo '<a href="' . home_url() . '" rel="nofollow">Home</a>';

        //for news detail page
        if(isset($atts['page']) && $atts['page'] == 'news'){

            echo "<span> <i class='fa fa-angle-right'></i> </span>";
            echo '<a href="/news" rel="nofollow">News</a>';
        }

        //for award detail page
        if(isset($atts['page']) && $atts['page'] == 'awards'){

            echo "<span> <i class='fa fa-angle-right'></i> </span>";
            echo '<a href="/awards" rel="nofollow">Awards</a>';
        }

        if (!empty($ancestors)) {

            $ancestors = array_reverse($ancestors);

            foreach ($ancestors as $key => $val) {

                echo "<span> <i class='fa fa-angle-right'></i> </span>";

                echo '<a href="' . get_permalink($val) . '">' . get_the_title($val) . '</a>';
            }
        }

        //show current page
        if(!is_archive()){
            echo "<span> <i class='fa fa-angle-right'></i> </span>";
            echo '<a href="'.get_permalink().'" class="active">'.$page_title.'</a>';
        }

        echo '</div></div>';
    }
}
add_shortcode('breadcrumbs', 'the_breadcrumb');

//For Award winner breadcrumb
function the_breadcrumbs_award_inner($atts='') {
    
    echo '<div class="breadcrumbs"><div class="container">';

    global $wp_query;
    $post_id = $wp_query->get_queried_object_id();
    
    echo '<a href="' . home_url() . '" rel="nofollow">Home</a>';

    echo "<span> <i class='fa fa-angle-right'></i> </span>";
    echo '<a href="/awards" rel="nofollow">Awards</a>';
    
    if($atts['award_id'] != ''){
        echo "<span> <i class='fa fa-angle-right'></i> </span>";
        echo '<a href="' . get_permalink($atts['award_id']) . '" rel="nofollow">'. get_the_title($atts['award_id']).'</a>';
    }

    echo "<span> <i class='fa fa-angle-right'></i> </span>";
    echo '<a href="' . get_permalink($post_id) . '" class="active">'.$atts['year'].' Winner</a>';

    echo '</div></div>';
}
add_shortcode('breadcrumb-inner', 'the_breadcrumbs_award_inner');
 

/**
 *
 * Used to return pagination in case of WP query does not meet requirement
 *
 * @param    $base_url  string url of site
 * @param    $query_str string used to add text as a GET parameter
 * @param    $total_pages int total number of records
 * @param    $current_page int current GET page param
 * @param    $paginate_limit int upto how many records to show after and before dots
 * @return   array
 *
 */
function customPagination($base_url, $page, $total_pages, $limit, $adjacents, $custompage = '') {
   
    $prev = $page - 1;
    $next = $page + 1;

    $lastpage = ceil($total_pages / $limit);
    $lpm1 = $lastpage - 1;

    $pagination = "";
    
    $search_k = '/?s='.$_GET['s'].'&page=';

    if ($lastpage > 1) {
        //previous button
        if ($page > 1) $pagination .= "<li><a class='prev page-numbers' href='".$base_url.$custompage.$search_k.$prev."'>&nbsp;</a></li>";
        else $pagination .= "<li><a class=\"prev page-numbers\">&nbsp;</a></li>";

        //pages	
        if ($lastpage < 7 + ($adjacents * 2)) { //not enough pages to bother breaking it up
            for ($counter = 1; $counter <= $lastpage; $counter++) {
                if ($counter == $page) $pagination .= "<li><a class=\"page-numbers current\">$counter</a></li>";
                else $pagination .= "<li><a class=\"page-numbers\" href='".$base_url.$custompage.$search_k.$counter."'>$counter</a></li>";
            }
        }
        elseif ($lastpage > 5 + ($adjacents * 2)) { //enough pages to hide some
            //close to beginning; only hide later pages
            if ($page < 1 + ($adjacents * 2)) {
                for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++) {
                    if ($counter == $page) $pagination .= "<li><a class=\"page-numbers current\">$counter</a></li>";
                    else $pagination .= "<li><a class=\"page-numbers\" href='".$base_url.$custompage.$search_k.$counter."'>$counter</a></li>";
                }
                $pagination .= "<li><a class=\"page-numbers\">...</a></li>";
                $pagination .= "<li><a class=\"page-numbers\" href='".$base_url.$custompage.$search_k.$lpm1."'>$lpm1</a></li>";
                $pagination .= "<li><a class=\"page-numbers\" href='".$base_url.$custompage.$search_k.$lastpage."'>$lastpage</a></li>";
            }
            //in middle; hide some front and some back
            elseif ($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2)) {
                $pagination .= "<li><a class=\"page-numbers\" href='".$base_url.$custompage.$search_k.'1'."'>1</a></li>";
                $pagination .= "<li><a class=\"page-numbers\" href='".$base_url.$custompage.$search_k.'2'."'>2</a></li>";
                $pagination .= "<li><a class=\"page-numbers\">...</a></li>";
                for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++) {
                    if ($counter == $page) $pagination .= "<li><a class=\"page-numbers current\">$counter</a>";
                    else $pagination .= "<li><a class=\"page-numbers\" href='".$base_url.$custompage.$search_k.$counter."'>$counter</a></li>";
                }
                $pagination .= "<li><a class=\"page-numbers\">...</a></li>";
                $pagination .= "<li><a class=\"page-numbers\" href='".$base_url.$custompage.$search_k.$lpm1."'>$lpm1</a></li>";
                $pagination .= "<li><a class=\"page-numbers\" href='".$base_url.$custompage.$search_k.$lastpage."'>$lastpage</a></li>";
            }
            else {
                $pagination .= "<li><a class=\"page-numbers\" href='".$base_url.$custompage.$search_k.'1'."'>1</a></li>";
                $pagination .= "<li><a class=\"page-numbers\" href='".$base_url.$custompage.$search_k.'2'."'>2</a></li>";
                $pagination .= "<li><a class=\"page-numbers\">...</a></li>";
                
                for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++) {
                    if ($counter == $page) $pagination .= "<li><a class=\"page-numbers current\">$counter</a></li>";
                    else $pagination .= "<li><a class=\"page-numbers\" href='".$base_url.$custompage.$search_k.$counter."'>$counter</a></li>";
                }
            }
        }

        //next button
        if ($page < ($counter - 1)) $pagination .= "<li><a class=\"next page-numbers\" href='".$base_url.$custompage.$search_k.$next."'>&nbsp;</a></li>";
        else $pagination .= "<li><a class=\"next page-numbers\">&nbsp;</a></li>";
    }

    return $pagination;
}
 
/*set default image sizes*/
add_image_size( 'thumbnail', 220);
add_image_size( 'medium', 450);
add_image_size( 'large', 1000);

// Add custom sizes to WordPress Media Library
function aoa_image_sizes( $sizes ) {
    return array_merge( $sizes, array(
        'thumbnail' => __('Thumbnail'),
        'medium' => __('Medium'),
        'large' => __('Large'),
    ) );
}
add_filter( 'image_size_names_choose', 'aoa_image_sizes' );


//shortcode for event listing
add_shortcode('eventlist', 'eventlist_func');
function eventlist_func($atts) {
    
    $event_html = '';
    
    //get sorted list of events
    $events = getEvents();
    
    //first 3
    $events = array_slice($events, 0, 3); 
    
    if ( !empty($events) ) :
        
        $last_key = end(array_keys($events));
        foreach($events as $k => $v){
            
            $event_id = $v['ID'];
    
            $last_item = FALSE;
    
            //last post item
            if ($last_key == $k) {
                
                $last_item = TRUE;
            }
            
            //get post meta values
            $event_date = get_field('event_date', $event_id);
            $start_time = get_field('start_time', $event_id);
            $end_time = get_field('end_time', $event_id);
            $event_location = get_field('event_location', $event_id);
            $fonteva_link = get_field('fonteva_link', $event_id);
            $is_webinar = get_field('is_webinar', $event_id);
            
            $featured_img_url = get_the_post_thumbnail_url($event_id, 'full'); 
            $image_id = get_post_thumbnail_id( $event_id );
            if($featured_img_url == ''){
                $featured_img_url = '/wp-content/themes/aoa/images/no-image.jpg';
            }
                
            $event_html .= '<div class="upcoming-events">
                    
                    <div class="upcoming-events-education-thumb">
                        <a href="'.$fonteva_link.'" target="_blank"><img src="'.$featured_img_url.'" class="vc_single_image-img attachment-full" width="100" height="100" title="'.get_the_title($image_id).'" /></a>
                    </div>
                    
                    <div class="upcoming-events-con">
                          <h5><a href="'.$fonteva_link.'" target="_blank">'.get_the_title($event_id).'</a></h5>
                          <p>'.get_post_field('post_content', $event_id).'</p>
                      </div>
					  <div class="clear"></div>
                  </div>';
            
        } //foreach
        
    endif;
    
    return $event_html;
}

/**
 *
 * Used to get Events and sort them based on start date
 * @author Mohanish S
 * @return Array
 *
 */
function getEvents() {
    
    global $post;
    try {
        
        //get events
        $query_args = array(
            'post_type'=> 'events',
            'post_status' => 'publish',
            'order'    => 'DESC',
            'posts_per_page' => -1
        );

        $posts = get_posts($query_args);
        
        $event_arr = [];
        $today = time();
        
        if(!empty($posts)){
            
            foreach($posts as $k => $v){
                
                $event_start_date = get_field('event_date', $v->ID);
                $timestamp = strtotime($event_start_date);
                
                //it should be greater than today
                if($timestamp > $today){
                    $event_arr[$k]['ID'] = $v->ID;
                    $event_arr[$k]['event_sort_date'] = $timestamp;
                    $event_arr[$k]['event_start_date'] = $event_start_date;
                    $event_arr[$k]['event_title'] = get_the_title( $v->ID );
                }
            }
            
            //sort array
            @usort($event_arr, function($a, $b) {  //descending
                return $a["event_sort_date"] > $b["event_sort_date"]; 
            });
            
        }
        
        return $event_arr;
        
    } catch (\Exception $ex) {
        echo 'Caught exception: ', $ex->getMessage(), "\n";
    }
}

//shortcode for event listing on Homepage
add_shortcode('featured-events', 'eventfeature_func');
function eventfeature_func($atts) {
    
    $event_html = '';
    
    //get posts = post_type= awards and using particular category
    $args = array(
        'post_type' => 'events',
        'post_status' => 'publish',
        'posts_per_page' => -1,
        'orderby' => 'date',
        'order' => 'DESC'
    );
    
    $e_posts = get_posts($args);
    
    $event_arr = [];
    $today = time();
    
    //first sort the event by date
    if(!empty($e_posts)){
        
        foreach($e_posts as $k => $v){
            
            $event_start_date = get_field('event_date', $v->ID);
            $timestamp = strtotime($event_start_date);

            //it should be greater than today
            if($timestamp > $today){
                $event_arr[$k]['ID'] = $v->ID;
                $event_arr[$k]['event_sort_date'] = $timestamp;
            }
        }
        
        //sort array
        @usort($event_arr, function($a, $b) {  //descending
            return $a["event_sort_date"] > $b["event_sort_date"]; 
        });
    }
    
    //show events
    if(!empty($event_arr)){
        
        $event_arr = array_slice($event_arr, 0, 3); 
        
        $last_key = end(array_keys($event_arr));
        
        foreach($event_arr as $ek => $ev){
            
            $post_ID = $ev['ID'];
            
            $content = get_post_field('post_content', $post_ID);
            $content = mb_strimwidth($content, 0, 150, '... ');
            $content = strip_tags($content);

            //event date
            $event_date = get_field('event_date', $post_ID);
            
            //event link
            $fonteva_link = get_field('fonteva_link', $post_ID);
            if($fonteva_link == ''){
                $fonteva_link = '#';
            }

            $year = date("Y", strtotime($event_date));
            $date = date("d", strtotime($event_date));
            $month = date("M", strtotime($event_date));

            $event_html .= '<div class="featured-events '.($ek == $last_key ? 'last-border' : '').'">
                    <a href="'.$fonteva_link.'" target="_blank"><div class="events-date"><strong>'.$date.'</strong><strong>'.$month.'</strong><span>'.$year.'</span></div></a>
                <div class="featured-events-content">
                    <a href="'.$fonteva_link.'" target="_blank"><h5>'.get_the_title($post_ID).'</h5></a>
                    <p>'.$content.'</p>
                </div>
                <div class="clear"></div>
            </div>';
            
            
        }
    }
    
    return $event_html;
}

//shortcode for listing post type on listing page
add_shortcode('posttype-listing', 'func_posttype_listing');
function func_posttype_listing($atts) {
    
    global $wpdb, $wp_query;
    
    $html = '';
    
    $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
    
    $args = array(
        'post_type' => 'post',
        'post_status' => 'publish',
        'posts_per_page' => 2,
        'paged' => $paged
    );
    
    $posts = new WP_Query($args);
    $max_num_pages = $posts->max_num_pages;
 
    $html .= '<div class="listing-page">';
    
    if ($posts->have_posts()) : while ($posts->have_posts()) : $posts->the_post(); 
    
        $post_ID = $post->ID;
            
        $content = get_post_field('post_content', $post_ID);
        $content = mb_strimwidth($content, 0, 160, '... ');
        $content = strip_tags($content);
        
        $html .= '<div class="list-box">
                    <div class="listing-box-thumb col">
                        <a href="#"><img src="/wp-content/themes/otb/images/dummy-image.jpg" /></a>
                    </div>
                    <div class="listing-box-content">
                        <h4><a href="'.get_permalink().'">'.get_the_title().'</a></h4>
                        <h5>Attribute 1</h5>
                        <h6>Attribute 2</h6>
                        <p>'.$content.'</p>
                        <a class="link-with-right-arrow moreless-button" href="#readless">Read More</a> 
                    </div>
                    <div class="clear"></div>
                </div>';
        
        
    endwhile;
    endif;
    
    $big = 999999999; // need an unlikely integer
    $pages = paginate_links(array(
        'base' => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
        'format' => '?paged=%#%',
        'current' => max(1, get_query_var('paged')),
        'total' => $max_num_pages,
        'type' => 'array',
        'prev_text' => __(''),
        'next_text' => __(''),
        'mid_size' => 1,
    ));

    if (is_array($pages)) {
        $paged = ( get_query_var('paged') == 0 ) ? 1 : get_query_var('paged');
        $html .= '<div class="pagination-box"><div class="pagination"><ul>';
        foreach ($pages as $page) {
            $html .= "<li>$page</li>";
        }
        $html .= '</ul></div></div>';
    }
    
    $html .= '</div>';
    
    return $html;
}

//Removing Visual Composer & Shortcodes
function vc_fix_content( $content ) {
    $content = preg_replace("~(?:\[/?)[^/\]]+/?\]~s", '', $content);
    $content = preg_replace('~(?:\[/?).*?"]~s', '', $content);
    $content = preg_replace('(\\[([^[]|)*])', '', $content );
    $content = preg_replace('/\[(.*?)\]/', '', $content );
    return htmlentities( $content );
}

/**
 *
 * Used to check if member page and return flag
 * @author Mohanish S
 * @return Boolean
 *
 */
function isMemberPageFlag($post_id) {
    try {

        $no_member_flag = FALSE;

        //logic to redirect user to the login page if non-member
        $post_member_acess_roles = get_post_meta($post_id, '_members_access_role');

        if (!empty($post_member_acess_roles)) {

            if (!is_user_logged_in()) { //if not logged in
                $no_member_flag = TRUE;
            }

            if (is_user_logged_in()) { //then check roles if logged in
                $user = wp_get_current_user();
                $roles = (array) $user->roles;

                $no_member_flag = TRUE;
                foreach ($roles as $role) {
                    if (in_array($role, $post_member_acess_roles)) {
                        $no_member_flag = FALSE;
                        break;
                    }
                }
            }
        }

        return $no_member_flag;
    } catch (\Exception $ex) {
        error_log('Caught exception: ' . $ex->getMessage());
    }
}
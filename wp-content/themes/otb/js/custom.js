// Header Search Start
jQuery(document).ready(function () {
    jQuery('.header-search-button button').click(function () {
        jQuery(this).toggleClass('active-s');
        jQuery(".header-search form").slideToggle('1000');
        event.stopPropagation();
    });
});

jQuery(document).click(function () {
    jQuery(".header-search form").slideUp(400);
    jQuery('.header-search-button button').removeClass('active-s');
});
jQuery(".header-search form").click(function (e) {
    e.stopPropagation();
});
   
jQuery(document).ready(function () {
    jQuery(".mega-toggle-block").click(function () {
        jQuery("#mega-menu-primary li").removeClass("mega-toggle-on");
    })
});

// The function toggles more (hidden) text when the user clicks on "Read more". The IF ELSE statement ensures that the text 'read more' and 'read less' changes interchangeably when clicked on. https://codepen.io/royketelaar/pen/avWxve
jQuery(document).ready(function () {
	jQuery('.moreless-button').click(function() {
		jQuery('.moretext').slideToggle();
		if (jQuery('.moreless-button').text() == "Read More") {
			jQuery(this).text("Read Less")
		} else {
			jQuery(this).text("Read More")
		}
	});
});

/* Open in a new window or tab */
jQuery(function($) {
	"use strict";
		
	$(document).ready(function() {	
		$(function() {
			$('a[href$=".pdf"]').prop('target', '_blank');
			$('a[href$=".mp4"]').prop('target', '_blank');
			$('a[href$=".docx"]').prop('target', '_blank');
		});
	});
	
	var all_links = document.querySelectorAll('a');
	for (var i = 0; i < all_links.length; i++){
	   var a = all_links[i];
	   if(a.hostname != location.hostname) {
			   a.rel = 'noopener';
			   a.target = '_blank';
	   }
	}
});

//load URL in 0 size iFrame
jQuery(document).ready(function () {
    jQuery("#logout_btn").click(function () {
        var sf_logout_url = jQuery("#sf_logout_url").val();
        jQuery("#sfframe").attr("src", sf_logout_url);
    });
});